package com.example.lottiecomposetest

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.animateLottieCompositionAsState
import com.airbnb.lottie.compose.rememberLottieComposition

@Composable
fun AnimationScreen() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(all = 10.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        content = {
            val spec = LottieCompositionSpec.RawRes(resId = R.raw.idea_into_book_machine)

            val composition by rememberLottieComposition(spec = spec)

            val state = animateLottieCompositionAsState(
                composition = composition,
                iterations = LottieConstants.IterateForever
            )

            /* pass progress as a lambda */
            LottieAnimation(
                composition = composition,
                progress = {
                    state.progress
                }
            )
        }
    )
}